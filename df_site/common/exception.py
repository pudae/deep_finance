class BaseError(Exception):
  def __init__(self):
    super(BaseError, self).__init__()
    self._status_code = 500
    self._message = "Internal Error"

  @property
  def status_code(self):
    return self._status_code

  @property
  def message(self):
    return "[{}]: ".format(self.__class__.__name__) + self._message


class InvalidArguments(BaseError):
  def __init__(self, message=""):
    super(InvalidArguments, self).__init__()
    self._status_code = 400
    self._message = message


class LogicError(BaseError):
  def __init__(self, message=""):
    super(LogicError, self).__init__()
    self._status_code = 400
    self._message = message


class NotFound(BaseError):
  def __init__(self, message=""):
    super(NotFound, self).__init__()
    self._status_code = 404
    self._message = message


class InternalError(BaseError):
  def __init__(self, message=""):
    super(InternalError, self).__init__()
    self._status_code = 500
    self._message = message


if __name__ == "__main__":
  a = InvalidArguments("hahah")
  b = NotFound("hohoho")

  print(a.status_code, a.message)
  print(b.status_code, b.message)

