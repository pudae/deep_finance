from __future__ import unicode_literals
from __future__ import print_function
from __future__ import division

import os

import numpy as np
import pandas as pd


data_dict = {}


def _load_data(backtest_dir):
  data = {}

  preprocessed_dir = os.path.join(backtest_dir, 'preprocessed')

  # load returns
  returns_path = os.path.join(preprocessed_dir, 'returns.csv')
  data['returns'] = pd.read_csv(returns_path, parse_dates=True, index_col='date')

  # load weights
  weights_path = os.path.join(backtest_dir, 'weights.npy')
  data['weights'] = np.load(weights_path)

  # load trading dates
  dates_path = os.path.join(backtest_dir, 'dates.csv')
  data['dates'] = pd.read_csv(dates_path, index_col='date', parse_dates=True)

  # load universe
  universe_path = os.path.join(backtest_dir, 'universe.csv')
  data['universe'] = pd.read_csv(universe_path)

  # ticker to name
  code_path = os.path.join(backtest_dir, 'kospi.csv')
  data['codes'] = pd.read_csv(code_path, index_col='code')

  # revenue
  revenue_dict = {}
  for ticker in data['universe']['ticker']:
    csv_path = os.path.join(preprocessed_dir, 'transaction_{:06d}.csv'.format(ticker))
    df_ticker = pd.read_csv(csv_path, parse_dates=True, index_col='date')
    revenue_dict['{:06d}'.format(ticker)] = df_ticker
  data['revenue'] = revenue_dict

  return data


def get_data_dict():
  global data_dict
  return data_dict


def _get_data(backtest_dir):
  # data_dict = get_data_dict()
  global data_dict
  print(list(data_dict.keys()))
  data = data_dict.get(backtest_dir)
  if data is None:
    print('load backtest_dir', backtest_dir)
    data = _load_data(backtest_dir)
    data_dict[backtest_dir] = data

  return data_dict[backtest_dir]


def get_returns(backtest_dir, start_date, end_date):
  data = _get_data(backtest_dir)
  df = data['returns']

  bi, ei = df.index.slice_locs(start=start_date, end=end_date)
  weights = data['weights'][:, bi:ei]
  max_weights = np.max(weights, axis=0)

  df_returns = df[start_date:end_date]

  return df_returns, max_weights



def get_weights(backtest_dir, selected_date):
  data = _get_data(backtest_dir)

  dates = data['dates']
  universe = data['universe']
  codes = data['codes']
  weights = data['weights']

  assert len(dates) == weights.shape[1]

  i = 0
  if selected_date is not None:
    i = dates.index.get_loc(selected_date)

  selected_weights = weights[:, i]
  selected_dates = dates.index[i]

  universe = ['{:06d}({})'.format(c, codes.loc[c]['name']) for c in universe['ticker']]

  return selected_weights, selected_dates, universe


def _get_realized_revenue_single(df, start_date, end_date):
  df = df.loc[start_date:end_date]
  total_revenue = df['accumulated_revenue'][-1] - df['accumulated_revenue'][0]

  return total_revenue


def get_realized_revenue(backtest_dir, start_date, end_date):
  data = _get_data(backtest_dir)

  df_universe = data['universe']
  df_codes = data['codes']

  revenue_dict = data['revenue']
  revenues = []
  for ticker in df_universe['ticker']:
    df_ticker = revenue_dict['{:06d}'.format(ticker)]
    revenue = _get_realized_revenue_single(df_ticker, start_date, end_date)
    key = '{:06d}({})'.format(ticker, df_codes.loc[ticker]['name'])
    revenues.append((key, revenue))

  print('sum:', sum([v[1] for v in revenues]))
  return list(sorted(revenues, key=lambda v: v[1]))[::-1]


def get_volume_price(backtest_dir, ticker, start_date, end_date):
  print('backtest_dir:', backtest_dir)
  print('ticker:', ticker)
  print('date:', start_date, end_date)

  data = _get_data(backtest_dir)

  df = data['revenue']['{:06d}'.format(int(ticker))]
  df_dates = data['dates']
  df_universe = data['universe']

  bi, ei = df_dates.index.slice_locs(start=start_date, end=end_date)
  stock_idx = int(df_universe[df_universe['ticker'] == int(ticker)].index[0])
  weights = data['weights'][stock_idx, bi:ei]
  print('weights:', weights.shape)

  return df.loc[start_date:end_date], weights
