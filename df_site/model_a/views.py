import os
import json
import io
import pandas as pd

from django.shortcuts import render, reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth import authenticate, login

import common.exception as exception
from dl_models.model_factory import ModelFactory


def code_to_string(code):
  if isinstance(code, int):
    return '{:06d}'.format(code)
  return code


def index_view(request):
  print('=====================')
  print('request', request)
  if not request.user.is_authenticated:
    return HttpResponseRedirect(reverse('model_a:view_login'))

  return render(
      request, 'model_a/index.html',
      {'models': ModelFactory.get_models()})


def predict_view(request):
  print('=====================')
  print('request', request)
  if not request.user.is_authenticated:
    return HttpResponseRedirect(reverse('model_a:view_login'))

  if request.method != 'GET':
    return HttpResponseRedirect(reverse('model_a:index'))

  print("GET!!")
  return render(
      request, 'model_a/predict.html',
      {'models': ModelFactory.get_models()})


def login_view(request):
  print('=====================')
  print('[login]:', request)
  if request.method == 'GET':
    return render(request, 'model_a/login.html', {'model_names': ModelFactory.get_names()})

  print('=====================')
  print('POST items')
  for key, value in request.POST.items():
    print(key, value)

  username = request.POST['inputID']
  password = request.POST['inputPassword']
  user = authenticate(request, username=username, password=password)
  if user is not None:
    login(request, user)
    return HttpResponseRedirect(reverse('model_a:index'))

  # Return an 'invalid login' error
  # message.

  return HttpResponseRedirect(reverse('model_a:view_login'))


def compute_portions(close_prices, volumes):
  holdings = [p * v for p, v in zip(close_prices, volumes)]
  total_holding = sum(holdings)
  return [h / total_holding for h in holdings]


def compute_volumes(close_prices, target_portions, total_capital):
  volumes = [int(portion * total_capital) // price
             for price, portion in zip(close_prices, target_portions)]
  return volumes


def target_portfolio_view(request):
  print('current directory', os.getcwd())
  if not request.user.is_authenticated:
    return HttpResponse(json.dumps({"error": "not logged in"}))

  if request.method != 'POST':
    return HttpResponse(json.dumps({"error": "invalid method"}))

  print('=====================')
  print('POST items')
  for key, value in request.POST.items():
    print(key, value)

  try:
    csv_file = request.FILES['inputCsv']
    if not csv_file.name.endswith('.csv'):
      messages.error(request, 'File is not CSV type')
      print("File is not CSV type!!!")
      return HttpResponse(json.dumps({'error': 'File is not CSV type'}))

    # TODO: check file size
    file_data = csv_file.read()
    bytes_io = io.BytesIO(file_data)
    df = pd.read_csv(bytes_io, encoding='CP949')

    max_ticker_counts = int(request.POST.get('maxCounts', '30'))
    model_name = request.POST.get('selectModel', '')
    total_capital = int(request.POST.get('totalCapital', '1000000000').replace(',', ''))
    print('view:max_ticker_counts', max_ticker_counts)
    print('view:Model Name:', model_name)
    print('view:total_capital', total_capital)
    model = ModelFactory.get_portfolio_builder(model_name)

    target_date = request.POST['targetDate']
    input_volumes = [(code_to_string(row['code']), row['volume'], row['name'])
                     for _, row in df.iterrows()]
    universe = [v[0] for v in input_volumes]

    price_dicts = model.get_price_inputs_np(universe, target_date)
    close_prices = [int(v[-1][1]) for v in price_dicts['inputs_raw']]
    del price_dicts['inputs_raw']
    current_volumes = [v[1] for v in input_volumes]
    current_portions = compute_portions(close_prices, current_volumes)

    target_portions = model.inference(universe,
                                      current_portions,
                                      target_date,
                                      price_dicts,
                                      max_ticker_counts=max_ticker_counts)
    target_portions = [v[1] for v in target_portions]
    target_volumes = compute_volumes(close_prices, target_portions, total_capital)

    rows = []
    for cur, close, portion, volume, target_portion, target_volume, in zip(input_volumes,
                                                                           close_prices,
                                                                           current_portions,
                                                                           current_volumes,
                                                                           target_portions,
                                                                           target_volumes):
      rows.append({'code': cur[0],
                   'name': cur[2],
                   'close': close,
                   'current_portion': portion,
                   'current_volume': volume,
                   'target_portion': target_portion,
                   'target_volume': target_volume,
                   })

    print("Response!!!")
    return HttpResponse(json.dumps({'rows': rows}))

  except exception.BaseError as e:
    print('BaseError', e.message)
    messages.error(request, 'Exception:' + e.message)
    return HttpResponse(json.dumps({'error': e.message}))

  except Exception as e:
    print('Exception', e)
    messages.error(request, 'Unable to upload file. ' + repr(e))
    return HttpResponse(json.dumps({'error': repr(e)}))
