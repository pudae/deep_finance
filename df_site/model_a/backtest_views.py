import json
import datetime

from django.shortcuts import render, reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth import authenticate, login

from dl_models.model_factory import ModelFactory
from . import data_loader

# Create your views here.

def backtest_view(request):
  if not request.user.is_authenticated:
    return HttpResponseRedirect(reverse('model_a:view_login'))

  if request.method != 'GET':
    return HttpResponseRedirect(reverse('model_a:index'))

  models = ModelFactory.get_models()
  for model in models:
    if 'model' in request.GET and model['name'] == request.GET['model']:
      model['selected'] = 'selected'
    else:
      model['selected'] = ''

  return render(
      request, 'model_a/backtest.html',
      {'models': models})
      #{'models': ModelFactory.get_models()})


def returns_view(request):
  if request.method != 'GET':
    return HttpResponse(json.dumps({"error": "invalid method"}))

  if not request.user.is_authenticated:
    return HttpResponse(json.dumps({"error": "not logged in"}))

  model = request.GET['model']
  start_date = request.GET['start_date']
  end_date = request.GET['end_date']

  start_date = datetime.datetime.strptime(start_date, '%Y/%m/%d')
  end_date = datetime.datetime.strptime(end_date, '%Y/%m/%d')

  backtest_dir = ModelFactory.get_backtest_dir(model)
  print('backtest_dir:', backtest_dir)
  returns, max_weights = data_loader.get_returns(backtest_dir, start_date, end_date)

  ret = {}
  ret['dates'] = [d.strftime('%Y/%m/%d') for d in list(returns.index)]
  ret['returns'] = list(returns['returns'] / returns['returns'][0])
  ret['returns_bnh'] = list(returns['returns_bnh'] / returns['returns_bnh'][0])
  ret['returns_kospi'] = list(returns['returns_kospi'] / returns['returns_kospi'][0])
  ret['volumes'] = list(returns['trade_balance'])
  ret['max_weights'] = list(max_weights)

  return HttpResponse(json.dumps(ret))


def weights_view(request):
  if request.method != 'GET':
    return HttpResponse(json.dumps({"error": "invalid method"}))

  if not request.user.is_authenticated:
    return HttpResponse(json.dumps({"error": "not logged in"}))

  model = request.GET['model']
  selected_date = request.GET['selected_date']

  backtest_dir = ModelFactory.get_backtest_dir(model)
  weights, date, universe = data_loader.get_weights(backtest_dir, selected_date)

  ret = {}
  ret['weights'] = list(weights)
  ret['date'] = date.strftime('%Y/%m/%d')
  ret['universe'] = universe

  return HttpResponse(json.dumps(ret))


def revenue_rank_view(request):
  if request.method != 'GET':
    return HttpResponse(json.dumps({"error": "invalid method"}))

  if not request.user.is_authenticated:
    return HttpResponse(json.dumps({"error": "not logged in"}))

  model = request.GET['model']
  start_date = request.GET['start_date']
  end_date = request.GET['end_date']

  start_date = datetime.datetime.strptime(start_date, '%Y/%m/%d')
  end_date = datetime.datetime.strptime(end_date, '%Y/%m/%d')

  backtest_dir = ModelFactory.get_backtest_dir(model)
  revenues = data_loader.get_realized_revenue(backtest_dir, start_date, end_date)

  ret = {}
  ret['labels'] = [name for name, _ in revenues[:10] + revenues[-10:]]
  ret['revenues'] = [value for _, value in revenues[:10] + revenues[-10:]]

  return HttpResponse(json.dumps(ret))


def price_volume_view(request):
  if request.method != 'GET':
    return HttpResponse(json.dumps({"error": "invalid method"}))

  if not request.user.is_authenticated:
    return HttpResponse(json.dumps({"error": "not logged in"}))

  model = request.GET['model']
  start_date = request.GET['start_date']
  end_date = request.GET['end_date']
  ticker = request.GET['ticker']

  start_date = datetime.datetime.strptime(start_date, '%Y/%m/%d')
  end_date = datetime.datetime.strptime(end_date, '%Y/%m/%d')

  backtest_dir = ModelFactory.get_backtest_dir(model)
  df, weights = data_loader.get_volume_price(backtest_dir, ticker, start_date, end_date)

  ret = {}
  ret['dates'] = [d.strftime('%Y/%m/%d') for d in list(df.index)]
  ret['holding'] = list(df['holding'])
  ret['weights'] = list(weights)
  ret['close'] = list(df['close'])
  ret['accumulated_revenue'] = list(df['accumulated_revenue'] - df['accumulated_revenue'][0])

  return HttpResponse(json.dumps(ret))
