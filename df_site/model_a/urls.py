"""df_site URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from . import views
from . import backtest_views

app_name = 'model_a'
urlpatterns = [
    path('', views.index_view, name='index'),

    # predict
    path('login', views.login_view, name='view_login'),
    path('predict', views.predict_view, name='view_predict'),
    path('api/target_portfolio', views.target_portfolio_view, name='api_target_portfolio'),

    # backtest
    path('backtest', backtest_views.backtest_view, name='view_backtest'),
    path('api/backtest_returns', backtest_views.returns_view, name='api_backtest_returns'),
    path('api/backtest_weights', backtest_views.weights_view, name='api_backtest_weights'),
    path('api/backtest_revenue_rank', backtest_views.revenue_rank_view, name='api_backtest_revenue_rank'),
    path('api/backtest_price_volume', backtest_views.price_volume_view, name='api_backtest_price_volume'),
]

