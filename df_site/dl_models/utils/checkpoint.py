# E0401: Unable to import
# pylint: disable=E0401

import os

import torch

def get_last_checkpoint(checkpoint_dir):
  checkpoints = [checkpoint for checkpoint in os.listdir(checkpoint_dir)
                 if checkpoint.endswith('.pth') and False == checkpoint.startswith('best')]
  if len(checkpoints) == 0:
    return None
  return os.path.join(checkpoint_dir, list(sorted(checkpoints))[-1])


def load_checkpoint(config, module_dict, checkpoint_path=None):
  if checkpoint_path is not None:
    print('load ', checkpoint_path)
    loaded_dict = torch.load(checkpoint_path)
    checkpoint = {'model': loaded_dict['model'], 'steps': loaded_dict['steps']}
  else:
    checkpoint_dir = os.path.join(config.train.dir, 'checkpoint')
    checkpoint_path = get_last_checkpoint(checkpoint_dir)
    if checkpoint_path is not None:
      checkpoint = torch.load(checkpoint_path)
    elif config.train.pretrained_path is not None:
      checkpoint = {'model': torch.load(config.train.pretrained_path)['model']}
    else:
      print('no checkpoint')
      return 0

  for key, module in module_dict.items():
    if key in checkpoint:
      print('load', key, 'from checkpoint')
      module.load_state_dict(checkpoint[key])
    else:
      print(key, 'is not in checkpoint')

  return checkpoint['steps'] if 'steps' in checkpoint else 0

