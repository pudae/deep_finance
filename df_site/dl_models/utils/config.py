from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# E0401: Unable to import
# pylint: disable=E0401

from easydict import EasyDict as edict

def get_default():
  c = edict()

  ################################################################################################
  # model configuration
  ################################################################################################
  c_model = edict()
  c_model.name = 'PortfolioConstructor'
  c_model.params = edict()
  c_model.params.num_tickers = 200
  c_model.params.input_size = 8
  c_model.params.hidden_size = 128
  c_model.params.output_size = 1
  c_model.params.num_layers = 4
  c.model = c_model

  ################################################################################################
  # train configuration
  ################################################################################################
  c_train = edict()
  c_train.dir = '/tmp/price_predict'
  c_train.pretrained_path = None
  c_train.learning_rate = 0.001
  c_train.batch_size = 1
  c_train.save_interval_steps = 1000
  c_train.log_every_n_steps = 100
  c_train.num_of_steps = 100000000
  c_train.num_grad_acc = None

  c_train.optimizer = edict()
  c_train.optimizer.name = 'sgd'
  c_train.optimizer.params = edict()

  c_train.scheduler = edict()
  c_train.scheduler.name = 'fixed'
  c_train.scheduler.params = edict()

  c_train.loss = edict()
  c_train.loss.names = ['ReturnsLoss']
  c_train.loss.params = edict()

  c.train = c_train

  ################################################################################################
  # dataset configuration
  ################################################################################################
  c_dataset = edict()
  c_dataset.name = 'PriceDataset'
  c_dataset.ticker_filename = None
  c_dataset.dir = ''

  c_dataset.input_days = 250
  c_dataset.date_fmt = '%Y-%m-%d'

  c_dataset.train = edict()
  c_dataset.train.date_begin = '2000-01-01'
  c_dataset.train.date_end = '2015-12-31'
  c_dataset.train.num_sampling_in_epoch = 500
  c_dataset.train.params = edict()

  c_dataset.validation = edict()
  c_dataset.validation.date_begin = '2016-01-01'
  c_dataset.validation.date_end = '2016-12-31'
  c_dataset.validation.num_sampling_in_epoch = 1
  c_dataset.validation.params = edict()

  c_dataset.test = edict()
  c_dataset.test.date_begin = '2017-01-01'
  c_dataset.test.date_end = '2017-12-31'
  c_dataset.test.num_sampling_in_epoch = 1
  c_dataset.test.params = edict()

  c_dataset.db = edict()

  c.dataset = c_dataset

  ################################################################################################
  # preprocessing configuration
  ################################################################################################
  c_preprocessing = edict()
  c_preprocessing.name = None
  c_preprocessing.num_threads = 8

  c.preprocessing = c_preprocessing

  return c


def _merge(src, dst):
  if not isinstance(src, edict):
    return

  for k, v in src.items():
    if isinstance(v, edict) and k in dst and isinstance(dst[k], edict):
      _merge(src[k], dst[k])
    else:
      dst[k] = v


def load(filename):
  import yaml
  print('load ', filename)
  with open(filename, 'r') as fid:
    yaml_config = edict(yaml.load(fid))

  config = get_default()
  _merge(yaml_config, config)

  return config
