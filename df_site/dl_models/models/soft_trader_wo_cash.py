# Copyright 2018 pudae. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

from __future__ import unicode_literals
from __future__ import print_function
from __future__ import division

import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import Parameter
from torch.autograd import Variable


class SoftTraderWoCash(nn.Module):
  def __init__(self):
    super(SoftTraderWoCash, self).__init__()

    def _init():
      v = np.random.normal(0, 0.001)
      while v == 0.0:
        v = np.random.normal(0, 0.001)
      return v

    self.zero = Variable(torch.FloatTensor([0.0])).cuda()
    self.s0 = Parameter(torch.FloatTensor([_init()]))
    self.s1 = Parameter(torch.FloatTensor([_init()]))
    self.c0 = Parameter(torch.FloatTensor([_init() + 1.0]))
    self.b0 = Parameter(torch.FloatTensor([_init() + 1.0]))
    self.tau = Parameter(torch.FloatTensor([_init()]))

    # self.a0 = Parameter(torch.FloatTensor([_init()]))
    # self.a1 = Parameter(torch.FloatTensor([_init()]))
    # self.c1 = Parameter(torch.FloatTensor([_init()]))
    # self.b1 = Parameter(torch.FloatTensor([_init()]))

  def compute_weights(self, inputs, weights_prev):
    # inputs: NUM_TICKERS
    # weights: NUM_TICKERS
    yt = inputs
    if weights_prev.size(0) > inputs.size(0):
      weights_prev = weights_prev[:inputs.size(0)]

    weights = weights_prev.clone()

    yt_mean = torch.mean(yt)
    gt = F.sigmoid(self.s0 * (yt - self.c0 * yt_mean))
    bt = F.sigmoid(self.s1 * (self.b0 * yt_mean - yt))

    remain = (1.0 - F.sigmoid(self.tau) * bt) * weights_prev

    kt = torch.sum(weights_prev - remain)

    st = torch.sum(gt)
    buy_wt = gt / st * kt
    weights = remain + buy_wt

    weights_sum = torch.sum(weights)
    weights = weights / weights_sum

    return {'target': weights, 'remain': remain, 'buy': buy_wt}


  def forward(self, inputs, labels):
    # inputs: BATCH_SIZE x NUM_TICKERS x LENGTH x CHANNEL
    assert inputs.size() == labels.size()
    batch_size, num_tickers, length, channel = inputs.size()
    # assert batch_size == 1
    assert channel == 1
    inputs = torch.squeeze(inputs, dim=3)
    labels = torch.squeeze(labels, dim=3)

    initial_weights = Variable(torch.FloatTensor([[1.0 / num_tickers] * num_tickers] * batch_size))

    initial_weights = initial_weights.cuda()
    weights_prev = initial_weights

    weights_list = []
    gt_list = []
    bt_list = []
    kt_list = []
    deltat_list = []

    for t in range(length):
      yt = inputs[:, :, t]         # yt: (BATCH_SIZE, NUM_TICKERS,)
      rt = labels[:, :, t]         # rt: (BATCH_SIZE, NUM_TICKERS,)

      weights = weights_prev.clone()

      # stage 1
      yt_mean = torch.mean(yt, dim=1, keepdim=True)
      gt = F.sigmoid(self.s0 * (yt - self.c0 * yt_mean))
      bt = F.sigmoid(self.s1 * (self.b0 * yt_mean - yt))
      gt_list.append(gt)
      bt_list.append(bt)

      # stage 2
      kt = torch.sum(F.sigmoid(self.tau) * bt * weights_prev, dim=1, keepdim=True)
      assert torch.ge(kt, 0.0).all(), kt.data
      kt_list.append(kt)

      # stage 3
      st = torch.sum(gt, dim=1, keepdim=True)
      buy_wt = gt / st * kt
      weights = weights_prev * (1.0 - F.sigmoid(self.tau) * bt) + buy_wt
      weights_sum = torch.sum(weights, dim=1, keepdim=True)

      # renorm
      assert torch.ge(weights, 0.0).all(), weights.data
      assert torch.le(weights, 1.00001).all(), weights.data

      weights = weights / weights_sum
      weights_sum = torch.sum(weights, dim=1, keepdim=True)

      assert torch.ge(weights, 0.0).all(), weights.data
      assert torch.le(weights, 1.0).all(), weights.data
      assert torch.le(weights_sum, 1.00001).all(), weights_sum.data[0]

      weights_list.append(weights)

      returns = rt
      rw = torch.mul(weights, returns)
      rw_sum = torch.sum(rw, dim=1, keepdim=True)
      weights_prev = torch.div(rw, rw_sum)

    # BATCH_SIZE x NUM_ASSETS x LENGTH
    weights_sequence = torch.stack(weights_list, dim=2)
    weights_sequence.unsqueeze_(3)

    goodness_sequence = torch.stack(gt_list, dim=2)
    badness_sequence = torch.stack(bt_list, dim=2)
    sell_amount_sequence = torch.stack(kt_list, dim=2)

    endpoints = {'weights': weights_sequence,
                 'goodness': goodness_sequence,
                 'badness': badness_sequence,
                 'sell_amount': sell_amount_sequence}

    return weights_sequence, endpoints
