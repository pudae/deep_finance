from __future__ import unicode_literals
from __future__ import print_function
from __future__ import division

# W0611: Unused import
# pylint: disable=W0611

from .portfolio_constructor_4 import PortfolioConstructor4


def get_model(config):
  constructor = globals().get(config.name)
  num_tickers = config.params.num_tickers
  input_size = config.params.input_size
  hidden_size = config.params.hidden_size
  output_size = config.params.output_size
  num_layers = config.params.num_layers
  return constructor(num_tickers, input_size, hidden_size, output_size, num_layers)
