from __future__ import unicode_literals
from __future__ import print_function
from __future__ import division

import torch.nn as nn

from .feature_extractor_4 import FeatureExtractor4
from .soft_trader_wo_cash import SoftTraderWoCash

class PortfolioConstructor4(nn.Module):
  def __init__(self, num_tickers, input_size, hidden_size, output_size, num_layers):
    super(PortfolioConstructor4, self).__init__()

    self.feature_extractor = FeatureExtractor4(num_tickers, input_size,
                                               hidden_size, output_size,
                                               num_layers)
    self.trader = SoftTraderWoCash()

  def forward(self, inputs, labels=None, **_):
    batch_size, num_tickers, _, _ = inputs.size()

    hiddens = self.feature_extractor.init_hidden(batch_size, num_tickers)
    desirabilities, endpoints_1 = self.feature_extractor.forward(inputs, hiddens)
    endpoints = endpoints_1

    if labels is not None:
      weights_sequence, endpoints_2 = self.trader.forward(desirabilities, labels)
      endpoints.update(endpoints_2)
    else:
      weights_sequence = None

    return weights_sequence, desirabilities, endpoints
