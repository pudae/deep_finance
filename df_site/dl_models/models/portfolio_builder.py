import os
import datetime

import numpy as np
import torch

import dl_models.utils.config
import dl_models.utils.checkpoint
from dl_models.datasets import get_dataset
from .model_factory import get_model

class PortfolioBuilder(object):
  def __init__(self, config_filepath):
    self.config = dl_models.utils.config.load(config_filepath)
    self.models = self.load_models()
    pass

  def inference(self, universe, current_portions, target_date, price_dicts=None,
                max_ticker_counts=30):
    print('max_ticker_counts:', max_ticker_counts)
    with torch.no_grad():
      price_dicts = {key:torch.FloatTensor(value).cuda().unsqueeze(0)
                     for key, value in price_dicts.items()
                     if value is not None}

    with torch.no_grad():
      current_portions = torch.FloatTensor(np.array(current_portions)).cuda()

    desirabilities_list = [model.forward(**price_dicts)[1] for model in self.models]
    desirability_list = [desirabilities[0, :, -1, 0]
                         for desirabilities in desirabilities_list]

    portions = current_portions #self.get_portions(current_portions)

    remain_list = []
    buy_list = []
    for model, desirability in zip(self.models, desirability_list):
      weights_dict = model.trader.compute_weights(desirability, portions)
      remain_list.append(weights_dict['remain'])
      buy_list.append(weights_dict['buy'])

    # 잔여비중
    remain = torch.mean(torch.stack(remain_list), dim=0)
    _, bottomk_indices = torch.topk(remain, portions.size(0) - (max_ticker_counts // 2),
                                    largest=False)
    remain[bottomk_indices] = 0.0

    # 매수 비중
    buy = torch.mean(torch.stack(buy_list), dim=0)
    _, buy_bottomk_indices = torch.topk(buy, portions.size(0) - (max_ticker_counts // 2),
                                        largest=False)
    buy[buy_bottomk_indices] = 0.0

    target_portions = remain + buy
    target_portions_sum = torch.sum(target_portions)
    target_portions = target_portions / target_portions_sum

    target_portions = [(code, portion.item())
                       for code, portion in zip(universe, target_portions)]
    return target_portions

  def load_models(self):
    networks = []
    for phase in range(100):
      for step in range(10, -1, -1):
        checkpoint_path = os.path.join(self.config.train.dir,
                                       '{:02d}'.format(phase),
                                       'window_{:02d}'.format(step),
                                       'checkpoint',
                                       'best.pth')
        if os.path.exists(checkpoint_path):
          break

      print('try checkpoint path', checkpoint_path)
      if not os.path.exists(checkpoint_path):
        print('not existent', checkpoint_path)
        break

      network = get_model(self.config.model)
      network = network.cuda()
      dl_models.utils.checkpoint.load_checkpoint(self.config,
                                       {'model': network},
                                       checkpoint_path=checkpoint_path)

      network.eval()
      networks.append(network)

    return networks

  def get_price_inputs_np(self, universe, target_date):
    DATE_FMT = '%Y-%m-%d'
    target_date = datetime.datetime.strptime(target_date, DATE_FMT)
    begin_date = target_date - datetime.timedelta(days=241)
    end_date = target_date - datetime.timedelta(days=1)
    print(begin_date, '~', end_date, '=>', target_date)

    dataset = get_dataset(self.config.dataset, universe, begin_date, end_date,
                          self.config.dataset.db)
    return dataset.get_price_inputs(universe)

  def get_price_inputs(self, universe, target_date):
    DATE_FMT = '%Y-%m-%d'
    target_date = datetime.datetime.strptime(target_date, DATE_FMT)
    begin_date = target_date - datetime.timedelta(days=241)
    end_date = target_date - datetime.timedelta(days=1)
    print(begin_date, '~', end_date, '=>', target_date)

    dataset = get_dataset(self.config.dataset, universe, begin_date, end_date,
                          self.config.dataset.db)
    input_dict = dataset.get_price_inputs(universe)
    del input_dict['inputs_raw']

    with torch.no_grad():
      return {key:torch.FloatTensor(value).cuda().unsqueeze(0)
              for key, value in input_dict.items()
              if value is not None}

  @classmethod
  def get_portions(cls, portions):
    portions = [v[1] for v in portions]
    with torch.no_grad():
      return torch.FloatTensor(portions).cuda()

