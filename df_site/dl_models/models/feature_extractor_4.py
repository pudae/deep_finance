from __future__ import unicode_literals
from __future__ import print_function
from __future__ import division

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable


class FeatureExtractor4(nn.Module):
  def __init__(self, num_tickers, input_size, hidden_size, output_size, num_layers):
    super(FeatureExtractor4, self).__init__()

    self.num_tickers = num_tickers
    self.hidden_size = hidden_size
    self.num_layers = num_layers
    print('-------------------------------------------------------------------------------------')
    print('FeatureExtractor4')
    print('num_tickers', num_tickers)
    print('hidden_size', hidden_size)
    print('num_layers', num_layers)

    self.lstm = nn.LSTM(input_size, hidden_size, num_layers=num_layers, batch_first=True)
    self.conv_01 = nn.Conv1d(in_channels=hidden_size,
                             out_channels=output_size,
                             kernel_size=1)

  def forward(self, inputs, hiddens):
    batch_size, num_tickers, length, num_features = inputs.size()
    inputs = inputs.view(-1, length, num_features)

    features, (hiddens, _) = self.lstm(inputs, hiddens)
    # features: BATCH_SIZE * NUM_TICKERS x LENGTH x CHANNEL
    #            -> BATCH_SIZE * NUM_TICKERS x CHANNEL x LENGTH
    features = features.transpose(1, 2).contiguous()
    features = self.conv_01(features)

    # features: BATCH_SIZE * NUM_TICKERS x CHANNEL x LENGTH
    #            -> BATCH_SIZE x NUM_TICKERS * CHANNEL x LENGTH
    features = features.view(batch_size, -1, length)

    # features: BATCH_SIZE x NUM_TICKERS * CHANNEL x LENGTH
    #            -> BATCH_SIZE x NUM_TICKERS x CHANNEL x LENGTH
    #            -> BATCH_SIZE x NUM_TICKERS x LENGTH x CHANNEL
    features = features.view(batch_size, num_tickers, -1, length)
    features = features.transpose(2, 3).contiguous()
    features = F.tanh(features) + 1.0
    endpoints = {'desirability': features}
    return features, endpoints

  def init_hidden(self, batch_size, num_tickers):
    return (Variable(torch.zeros(self.num_layers, batch_size * num_tickers, self.hidden_size)).cuda(),
            Variable(torch.zeros(self.num_layers, batch_size * num_tickers, self.hidden_size)).cuda())

