# E0401: Unable to import
# pylint: disable=E0401

import datetime
import os
import itertools
from collections import defaultdict

import mysql.connector as mariadb
import numpy as np
import pandas as pd
import sklearn.preprocessing

from torch.utils.data import Dataset

import common.exception as exception


class PriceSamplingDatasetDB(Dataset):  # pylint: disable=R0902
  def __init__(self,
               universe,
               dataset_dir,
               date_fmt='%Y-%m-%d',
               date_begin=None,
               date_end=None,
               transform=None,
               db_config=None):
    assert isinstance(universe, list)

    self.universe = universe
    self.dataset_dir = dataset_dir
    self.db_config = db_config

    self.date_fmt = date_fmt
    if isinstance(date_begin, str):
      date_begin = datetime.datetime.strptime(date_begin, date_fmt)
    if isinstance(date_end, str):
      date_end = datetime.datetime.strptime(date_end, date_fmt)

    assert isinstance(date_begin, datetime.datetime)
    assert isinstance(date_end, datetime.datetime)
    self._date_begin = date_begin.date()
    self._date_end = date_end.date()
    print('-------------------------------------------------------------------')
    print('date range, [{}, {}]'.format(self._date_begin, self._date_end))

    self.scailer = sklearn.preprocessing.MinMaxScaler()
    self._preload()             # data preloading

  @property
  def date_begin(self):
    return self._date_begin

  @property
  def date_end(self):
    return self._date_end

  def _preload(self):
    print('loading..')
    try:
      conn = mariadb.connect(host=self.db_config.host,
                             user=self.db_config.user,
                             port=self.db_config.port,
                             password=self.db_config.password,
                             db=self.db_config.name,
                             charset=self.db_config.charset)
      self.trading_dates = self._load_trading_dates(conn)
      df_dict = {ticker:self._load_dataframe(ticker, conn) for ticker in self.universe}
      print('trading dates', self.trading_dates[0], self.trading_dates[-1])
      self.df_dict = {ticker:self._reindex(self.trading_dates, ticker, df)
                      for ticker, df in df_dict.items()}

      self.df_dict = {ticker:self._slice(ticker, df)
                      for ticker, df in df_dict.items()}
    except mariadb.Error as e:
      print('====================')
      print(e)
      raise e
    finally:
      if conn is not None:
        conn.close()

  def _reindex(self, dates_indices, ticker, df_ticker):
    # 거래일 기준으로 인덱싱 다시 함
    df_dates = pd.DataFrame(index=dates_indices)

    if len(df_ticker) == 0:
      # 빈 ticker는 1.0으로 채워 넣음
      for key in ['open', 'close', 'low', 'high', 'mean05', 'mean20', 'mean60']:
        df_dates[key] = 1.0
      df_dates['volume'] = 0.0
      df_dates['daily_return'] = 1.0
      df_ticker = df_dates
    else:
      df_ticker = df_dates.join(df_ticker)

    for key in ['open', 'close', 'low', 'high', 'mean05', 'mean20', 'mean60']:
      df_ticker[key] = df_ticker[key].fillna(method='ffill')
      df_ticker[key] = df_ticker[key].fillna(method='bfill')
      df_ticker[key] = df_ticker[key].fillna(1.0)
    df_ticker['volume'] = df_ticker['volume'].fillna(0.0)
    df_ticker['daily_return'] = df_ticker['daily_return'].fillna(1.0)
    return df_ticker

  def _slice(self, ticker, df):
    indexer = df.index.slice_indexer(self._date_begin, self._date_end)
    ret_df = df[indexer]
    if len(ret_df) == 0:
      print('----------------------------------')
      print('ticker:', ticker)
      print(ret_df.head())
    return ret_df

  def _validate_dates(self, df):
    assert df.index[0] >= self._date_begin, '{} vs {}'.format(df.index[0], self._date_begin)
    assert df.index[-1] <= self._date_end, '{} vs {}'.format(df.index[-1], self._date_end)
    return df

  def __len__(self):
    pass

  def __getitem__(self, index):
    pass

  def _get_inputs(self, df):
    return df[['open', 'close', 'high', 'low', 'mean05', 'mean20', 'mean60', 'volume']].values

  def _get_labels(self, df):
    return df[['daily_return']].values

  def get_price_inputs(self, universe, length=120):
    inputs = []
    for ticker in universe:
      df = self.df_dict[ticker]
      self._validate_dates(df)
      inputs.append(self._get_inputs(df)[-length:])

    shape_dict = defaultdict(list)
    for ticker, v in zip(universe, inputs):
        shape_dict[v.shape].append(ticker)

    if len(shape_dict) > 1:
        sorted_shapes = list(sorted([(shape, tickers)
                                     for shape, tickers in shape_dict.items()],
                                    key=lambda v: len(v[1])))
        invalid_tickers = [v for _, v in sorted_shapes][:-1]
        invalid_tickers = list(itertools.chain.from_iterable(invalid_tickers))
        message = '{} has not enough price data'.format(invalid_tickers)
        raise exception.InternalError(message=message)


    inputs = np.stack(inputs, axis=0)
    print('input shape:', inputs.shape)
    assert np.isnan(inputs).any() == False, np.where(np.isnan(inputs))
    inputs_normalized = self._normalize(inputs)
    assert np.isnan(inputs_normalized).any() == False, np.where(np.isnan(inputs_normalized))
    return {'inputs':inputs_normalized.astype(np.float32),
            'inputs_raw':inputs.astype(np.float32)}

  def _normalize(self, inputs):
    inputs = np.copy(inputs)
    eps = 1e-10
    inputs[:, :, 0:4] /= (inputs[:, 0:1, 1:2] + eps) # 시가, 종가, 저가, 고가는 최초 종가로 normalize
    inputs[:, :, 4:7] /= (inputs[:, 0:1, 4:7] + eps) # 5,20,60일 이동평균선은 최초 값으로 normalize
    inputs[:, :, 0:7] -= 1.0
    for i in range(inputs.shape[0]):
      inputs[i, :, 7:] = self.scailer.fit_transform(inputs[i, :, 7:])

    return inputs

  def get_trading_dates(self):
    return list(self.trading_dates)

  def get_universe(self):
    return self.universe

  def _load_trading_dates(self, conn):
    SQL = ('SELECT DISTINCT date'
           ' FROM price_adjust '
           ' WHERE date >= %(date_begin)s AND date <= %(date_end)s'
           ' ORDER BY date ASC')
    df = pd.read_sql(SQL, conn, index_col='date', parse_dates=True,
                     params={'date_begin': self._date_begin,
                             'date_end': self._date_end})
    print('========================================')
    print('_loading_trading_dates')
    print('HEAD')
    print(df.head(10))
    print('TAIL')
    print(df.tail(10))

    return df.index

  def _load_dataframe(self, ticker, conn):
    COLUMNS = ['date', 'ticker', 'open', 'close', 'high', 'low', 'volume', 'daily_return']
    COLUMNS_STR = ','.join(COLUMNS)
    SQL = ('SELECT ' + COLUMNS_STR + ' '
           ' FROM price_adjust '
           ' WHERE ticker = %(ticker)s AND date >= %(date_begin)s AND date <= %(date_end)s'
           ' ORDER BY date ASC')

    df = pd.read_sql(SQL, conn, index_col='date', parse_dates=True,
                     params={'ticker': ticker,
                             'date_begin': self._date_begin,
                             'date_end': self._date_end})

    for key in ['open', 'close', 'low', 'high']:
      df[key] = df[key].fillna(method='ffill')
      df[key] = df[key].fillna(method='bfill')

    df['daily_return'] = df['daily_return'].fillna(0.0)
    df['volume'] = df['volume'].fillna(method='ffill')
    df['volume'] = df['volume'].fillna(method='bfill')
    df['volume'] = df['volume'].fillna(0.0)

    df['mean05'] = df['close'].rolling(5).mean()
    df['mean20'] = df['close'].rolling(20).mean()
    df['mean60'] = df['close'].rolling(60).mean()

    df['daily_return'] = df['daily_return'] + 1.0

    # 이동 평균 60이 유효한 60일 이후부터 사용
    df = df.iloc[60:]                                               # pylint: disable=E1101

    return df

