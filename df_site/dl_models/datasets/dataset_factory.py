# E0401: Unable to import
# W0611: Unused import
# pylint: disable=E0401, W0611

import os

import pandas as pd

from .price_sampling_dataset_db import PriceSamplingDatasetDB


def get_dataset(config, universe, date_begin, date_end, db_config):
  assert universe is not None
  constructor = globals().get(config.name)

  dataset = constructor(universe,
                        config.dir,
                        date_begin=date_begin,
                        date_end=date_end,
                        db_config=db_config)

  return dataset
