from __future__ import unicode_literals
from __future__ import print_function
from __future__ import division

# W0611: Unused import
# pylint: disable=W0611

import copy

from django.conf import settings

from .models.portfolio_builder import PortfolioBuilder


def read_description(filepath):
  with open(filepath, 'r') as fid:
    return fid.read()


def load_model(setting):
  portfolio_builder = PortfolioBuilder(setting['config'])
  description = read_description(setting['description'])
  new_setting = copy.deepcopy(setting)
  new_setting['portfolio_builder'] = portfolio_builder
  new_setting['backtest_dir'] = setting['backtest']
  new_setting['description'] = description
  return new_setting


class ModelFactory(object):
  models = {v['name']:load_model(v) for v in settings.MODEL_CONFIGS}

  def __init__(self):
    pass

  @classmethod
  def get_names(cls):
    return cls.models.keys()

  @classmethod
  def get_models(cls):
    return list(cls.models.values())

  @classmethod
  def get_portfolio_builder(cls, name):
    return cls.models[name]['portfolio_builder']

  @classmethod
  def get_backtest_dir(cls, name):
    return cls.models[name]['backtest_dir']
