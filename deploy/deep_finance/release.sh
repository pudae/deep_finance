#!/bin/bash

set -ex

# set variables 
# registry_address="docker.deep.est.ai"
registry_address="127.0.0.1:5000"
image_name="deep_finance"
registry_tag=$registry_address/$image_name
patch_command=$1


# -------------------------------------------------------------------------------------------------
# check arguments
# -------------------------------------------------------------------------------------------------
if [ -z "$patch_command" ]; then
	patch_command="patch"
elif [[ ! "$patch_command" =~ ^(patch|minor|major)$ ]]; then
	echo 'Invalid arguments. [patch|minor|major]'
	exit 1
fi

echo "patch_command: $patch_command"


# -------------------------------------------------------------------------------------------------
# check if working directory is clean
# -------------------------------------------------------------------------------------------------
if [ -z "$(git status --porcelain --untracked-files=no)" ]; then
	echo 'Working directory is clean'
else
	echo 'There are uncommited changes'
	exit 1
fi


# -------------------------------------------------------------------------------------------------
# git pull
# -------------------------------------------------------------------------------------------------
git pull


# -------------------------------------------------------------------------------------------------
# bump version
# -------------------------------------------------------------------------------------------------
version_str=`cat VERSION`
IFS='.'; versions=($version_str); unset IFS;

major_version=${versions[0]}
minor_version=${versions[1]}
patch_version=${versions[2]}

if [[ "$patch_command" == "patch" ]]; then
  patch_version=$((patch_version + 1))
elif [[ "$patch_command" == "minor" ]]; then
  minor_version=$((minor_version + 1))
elif [[ "$patch_command" == "major" ]]; then
  major_version=$((major_version + 1))
else
	echo 'Invalid patch command'
	exit 1
fi

version_str=$major_version.$minor_version.$patch_version
echo $version_str > VERSION


# -------------------------------------------------------------------------------------------------
# build docker image
# -------------------------------------------------------------------------------------------------
docker build -t $registry_tag:latest -f Dockerfile ../..
 

# -------------------------------------------------------------------------------------------------
# tag 
# -------------------------------------------------------------------------------------------------
git add VERSION
git commit -m "version $version_str"
git tag -a "$image_name.$version_str" -m "$image_name :version $version_str"
git push
git push --tags

docker tag $registry_tag:latest $registry_tag:$version_str
 
# -------------------------------------------------------------------------------------------------
# push docker image
# -------------------------------------------------------------------------------------------------
docker push $registry_tag:latest
docker push $registry_tag:$version_str
